<?php

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

$templates = array( 'single-post.twig' );


//this page is child of an association page ?
if($children = association_child_menu()) {
    //array_unshift($templates, 'association-page.twig');
    $context['post']->children = $children;
}
require_once __DIR__ . '/_add-to-post.php';

//GLOBAL VARIABLE FOR TIMBER:
$context['post']->association_frontpage_id = current_association_front_page_id();

//inject parent page title to variable:
//does not work for events!
$context['associationMainPage'] = getAssociationMainPageTitle($post->ID);
$tmp_categories = wp_get_post_categories($post->ID);

if(!$context['associationMainPage'] && sizeof($tmp_categories) < 2) {
    $context['associationMainPage'] = getAssociationMainPageTitle($context['event_association_id']);
}



//Timber::render( $templates, $context );

Timber::render( $templates, $context );

