/* eslint-disable */
import $ from "jquery";
import Vue from "vue";

//import Articles from "./components/Articles.vue";
//import Posts from "./components/Posts.vue";
//import Projects from "./components/Projects.vue";
import lozad from "lozad";
import Rellax from "rellax";

// usage: log('inside coolFunc',this,arguments);
// http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function() {
  log.history = log.history || []; // store logs to an array for reference
  log.history.push(arguments);
  if (this.console) {
    window.console.log(Array.prototype.slice.call(arguments));
  }
};

/*
new Vue({
  el: "#app",
  components: { Articles, Posts, Projects }
});
*/

window.jQuery = window.$ = require("jquery");

//const observer = lozad();
//observer.observe();

let jQuery = $;

console.log($("body"));

/* FORM */

/*
optons:
1 = Yritys
2 = Henkilö
4 = Opiskelija

*/


var tradeassociation_data_fi = [];
var tradeassociation_data_en = [];
tradeassociation_data_fi = [
{ "id": 1, "name": "Finland Australia Business Council", "options": 3, "size_question": 1 },
{ "id": 2, "name": "Finland-Japan Chamber of Commerce", "options": 7, "size_question": 0 },
{ "id": 3, "name": "Finnish-British Business Association", "options": 3, "size_question": 0 },
{ "id": 4, "name": "Finnish-Canadian Business Council", "options": 3, "size_question": 0 },
{ "id": 5, "name": "Suomalais-Arabialainen kauppayhdistys", "options": 7, "size_question": 0 },
{ "id": 6, "name": "Suomalais-Eestiläinen kauppayhdistys", "options": 3, "size_question": 0 },
{ "id": 7, "name": "Suomalais-Latinalaisamerikkalainen kauppayhdistys", "options": 3, "size_question": 0 },
{ "id": 8, "name": "Suomi-Hong Kong kauppayhdistys", "options": 3, "size_question": 0 },
{ "id": 9, "name": "Suomi-Intia kauppayhdistys", "options": 3, "size_question": 0 },
{ "id": 10, "name": "Suomi-Iran kauppayhdistys", "options": 3, "size_question": 0 },
{ "id": 11, "name": "Suomi-Kaakkois-Aasia kauppayhdistys", "options": 3, "size_question": 1 },
{ "id": 12, "name": "Suomi-Keskisen Itäeuroopan maiden kauppayhdistys (Suomi-KIE)", "options": 3, "size_question": 0 },
{ "id": 13, "name": "Suomi-Kiina kauppayhdistys", "options": 1, "size_question": 0 },
{ "id": 14, "name": "Suomi-Korea kauppayhdistys", "options": 3, "size_question": 0 },
{ "id": 15, "name": "Suomi-Latvia kauppayhdistys", "options": 3, "size_question": 0 },
{ "id": 16, "name": "Suomi-Liettua kauppayhdistys", "options": 3, "size_question": 0 },
{ "id": 17, "name": "Suomi-Turkki kauppayhdistys", "options": 3, "size_question": 0 }
];

tradeassociation_data_en = [
{ "id": 1, "name": "Finland Australia Business Council", "options": 3, "size_question": 1 },
{ "id": 11, "name": "Finland – South East Asia Business Association", "options": 3, "size_question": 1 },
{ "id": 12, "name": "Finland-Central Eastern European Countries Business Association", "options": 3, "size_question": 0 },
{ "id": 13, "name": "Finland-China Business Association", "options": 1, "size_question": 0 },
{ "id": 8, "name": "Finland-Hong Kong Business Association", "options": 3, "size_question": 0 },
{ "id": 9, "name": "Finland-India Business Association (FIBA)", "options": 3, "size_question": 0 },
{ "id": 10, "name": "Finland-Iran Business Association", "options": 3, "size_question": 0 },
{ "id": 2, "name": "Finland-Japan Chamber of Commerce", "options": 7, "size_question": 0 },
{ "id": 14, "name": "Finland-Republic of Korea Business Association", "options": 3, "size_question": 0 },
{ "id": 17, "name": "Finland-Turkey Business Association", "options": 3, "size_question": 0 },
{ "id": 5, "name": "Finnish-Arab Business Association", "options": 7, "size_question": 0 },
{ "id": 3, "name": "Finnish-British Business Association", "options": 3, "size_question": 0 },
{ "id": 4, "name": "Finnish-Canadian Business Council", "options": 3, "size_question": 0 },
{ "id": 6, "name": "Finnish-Estonian Business Association", "options": 3, "size_question": 0 },
{ "id": 7, "name": "Finnish-Latin American Business Council", "options": 3, "size_question": 0 },
{ "id": 15, "name": "Finnish-Latvian Trade Association", "options": 3, "size_question": 0 },
{ "id": 16, "name": "Finnish-Lithuanian Business Association", "options": 3, "size_question": 0 }
];


if($('.citation-slide-container').length > 0) {
  var slider = tns({
    container: '.citation-slide-container',
    items: 1,
    autoplay: false,
    slideBy: 'page',
    controlsContainer:'#controls',
  });
}

if($('#tradeassociation').length) {
  console.log('membership form found');

  //initial hide
  $('.wpcf7-form .applicationtype-container').addClass('hidden-start');
  $('.wpcf7-form .generalfields').addClass('hidden-start');




  var $dropdown = $("#tradeassociation");

  /* dropdown data by the language */
  if($('.tradeassociation-fi').length > 0) {
    var tradeassociation_data = tradeassociation_data_fi;
  }
  else {
    var tradeassociation_data = tradeassociation_data_en;
  }

  $.each(tradeassociation_data, function() {
      $dropdown.append($("<option />").val(this.name).text(this.name).attr('data-options', this.options).attr('data-size_question', this.size_question));
  });

  if($('#tradeassociation').hasClass('en')) {
    $('#tradeassociation option:first').text('Choose business association...');
  }
  else {
    $('#tradeassociation option:first').text('Valitse kauppayhdistys...');
  }



  $dropdown.on('change', function() {
    //show again initially hidden:
    $('.wpcf7-form .applicationtype-container').removeClass('hidden-start');



    //clear radiobutton:
    $('input[name="applicationtype"]').prop('checked', false);


    var $sel = $("#tradeassociation option:selected")
    var thisOptions = $sel.attr('data-options');
    var sizeQuestion = $sel.attr('data-size_question');


    $('#applicationtype > span').show(); //show all

    if(thisOptions < 4) {
      /* hide last, student */
      console.log('hide student');
      $('#applicationtype > span:last').hide();
    }
    if(thisOptions == 5 || thisOptions == 4 || thisOptions == 1) {
      /* hide second, person */
      console.log('hide person')
      $('#applicationtype > span:eq(1)').hide();
    }


    let emptyField ='<span id="empty_company_size"><input type="radio" name="company_size" value="-" checked="checked"></span>';

    if(sizeQuestion == 0) {
      $('.company_size_container').hide();
      if($('#empty_company_size').length == 0) { /* only one */
        $('#company_size').append(emptyField); /*required field so we add empty value */
      }
    }
    else {
      $('.company_size_container').show();
      $('#empty_company_size').remove();  /*remove extra field */
    }


  });
  //dropdown change END

  $('.applicationtype').one('change', function() {
    //show all initially hidden fields:
    $('.hidden-start').removeClass('hidden-start');
  });
  //applicationtype change END


  $('.persongroup > a').on('click', function(e) {
    e.preventDefault();

    $(this).parent().find('.hidden').first().removeClass('hidden').addClass('visible');
    $(this).hide();
  });


}

//Add Sub-link is-active
$(".sidebar--links a").each(function(e){
  if (this.href == document.location.toString()) {
    $(this).addClass("active");
  }
});

//Show all events
if ($('.event-list').length > 4) {
  $('.event-list:gt(4)').hide();
  $('.show-more').show();
}

$('.show-more').on('click', function() {
  $('.event-list:gt(4)').toggle();
  $(this).hide()
});
//Show all news
if ($('.news-list').length > 4) {
  $('.news-list:gt(4)').hide();
  $('.show-more-news').show();
}

$('.show-more-news').on('click', function() {
  $('.news-list:gt(4)').toggle();
  $(this).hide()
});

//link fixed, no needed:
/*
$('.backlink-post, .backlink-event').on('click', function(e) {
  e.preventDefault();
  history.go(-1);
  return false;
});
*/


var rellax = new Rellax(".parallax_item",{
  speed: 4,
  center: !1,
  wrapper: null,
  round: !0,
  vertical: !0,
  horizontal: !1
});



/* fix safari z-index */

$('#main-container').css('-webkit-transform','translate3d(0px, 0px, 2px)');

setTimeout(function() {
  $('#main-container').css('-webkit-transform','translate3d(0px, 0px, 4px)');
}, 100);

setTimeout(function() {
  $('#main-container').css('-webkit-transform','translate3d(0px, 0px, 3px)');
}, 1000);

document.addEventListener( 'wpcf7mailsent', function( event ) {
      var formNode = document.getElementsByClassName('wpcf7-submit')[0];
      formNode.scrollIntoView();
}, false );
