if(wp && typeof wp.domReady ==='function') {

wp.domReady(() => {
  wp.blocks.registerBlockStyle('mp-blocks/mp-hero', [
      {
        name: 'mp-hero-button',
        label: 'Kuvablokkki nappilinkillä',
      }
    ]);
  wp.blocks.registerBlockStyle('mp-blocks/mp-two-columns', [
    // {
    //   name: '50-50',
    //   label: '50/50',
    //   isDefault: true,
    // },
    {
      name: '70-30',
      label: 'Teksti + linkkipainike',
    },
    {
      name: '70-30-medium',
      label: 'MediumTeksti + linkkipainike',
    },
    {
      name: '20-80',
      label: 'Ikoni + teksti',
    },
    {
      name: 'container--full-width-1',
      label: 'Puhekuplat taustakuvalla-1',
    },
    {
      name: 'bubbles',
      label: 'Puhekuplat ilman taustaa',
    }
  ]);
  wp.blocks.registerBlockStyle('mp-blocks/mp-introduction', [
      {
        name: 'text-huge-centered',
        label: 'Keskitetty',
      },
      {
        name: 'text-huge-line',
        label: 'Tasattu vasemmalle, pystyviivalla',
      },
      {
        name: 'text-huge-centered-background',
        label: 'Keskitetty animoidulla taustakuvalla',
      }
    ]);
  wp.blocks.registerBlockStyle('mp-blocks/mp-posts', [
      {
        name: 'grey-bg',
        label: 'Harmaa tausta',
      }
    ]);
  wp.blocks.registerBlockStyle('core/quote', [
      {
        name: 'blockquote-noname',
        label: 'Quote ilman nimeä',
      }
    ]);
  wp.blocks.registerBlockStyle('core/group', [
      {
        name: 'group-readmore',
        label: 'Lue lisää teksti...',
      }
    ]);
});

/*disable link in editor */
setTimeout(function() {
  jQuery('.edit-post-visual-editor .wp-block-mp-blocks-mp-highlight > a').each(function(e) {
    jQuery(this).prop("href", "#")
    /*jQuery(this).css('opacity', '0.1'); */
  });
}, 1200);


}

