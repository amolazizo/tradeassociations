
# Trade Associations

This is the web-site of the Business Associations operating in Finland effectively network companies, provide up-to-date information on the target country’s business environment and organize events for their internationalized and internationalization-planning member companies. 

The technologies and tools used to realize this application are as follows:
---------------------------------------------------------------------------

PHP, Wordpress, Timber, Twig, Javascript, HTML, CSS, SASS, Git, MySQL, Shell scripts.
