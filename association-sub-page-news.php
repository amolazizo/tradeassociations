<?php
/**
 * Template name: Association SubPage News
 * The template for displaying events in sub page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

$templates = array( 'association-page-news.twig' );

//this page is child of an association page ?
if($children = association_child_menu()) {
    //array_unshift($templates, 'association-page.twig');
    $context['post']->children = $children;
}

require_once __DIR__ . '/_add-to-post.php';

//GLOBAL VARIABLE FOR TIMBER:
$context['post']->association_frontpage_id = current_association_front_page_id();

//inject parent page title to variable:
$context['associationMainPage'] = getAssociationMainPageTitle($post->ID);

Timber::render( $templates, $context );
