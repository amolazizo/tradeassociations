<?php
/**
 * Template name: Association FrontPage
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();

$context['post'] = $post;

$templates = array('association-page.twig');
if (is_front_page()) {
    array_unshift($templates, 'home-page.twig');
}

require_once __DIR__ . '/_add-to-post.php';

//Timber::render( array( 'page-' . $post->post_name . '.twig', 'single.twig' ), $context );
//print_r($templates);
//die();

//GLOBAL VARIABLE FOR TIMBER:
$context['post']->association_frontpage_id = $post->ID;
$context['associationMainPage'] = getAssociationMainPageTitle($post->ID);
Timber::render($templates, $context);
