<?php
/**
 * Template Name: Posts Listing
 *
 * The Template for displaying all single posts
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();

// Set the buy terms footer true:
$context['show_download_footer'] = true;

$templates = array('post-listing.twig', 'single.twig');

$context['post'] = $post;

$more_posts = Timber::get_posts(
    array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'post__not_in' => array($post->ID),
        'posts_per_page' => '99',
    )
);

$context['more_posts'] = $more_posts;

Timber::render($templates, $context);
