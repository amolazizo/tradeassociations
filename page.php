<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$templates = array('index.twig');
if (is_front_page()) {
    array_unshift($templates, 'home-page.twig');
}
//Timber::render( array( 'page-' . $post->post_name . '.twig', 'single.twig' ), $context );
//print_r($templates);
//die();

//this page is child of an association page ?
if ($children = association_child_menu()) {
    array_unshift($templates, 'association-page.twig');
    $context['post']->children = $children;
}

require_once __DIR__ . '/_add-to-post.php';

//GLOBAL VARIABLE FOR TIMBER:
$context['post']->association_frontpage_id = current_association_front_page_id();

Timber::render($templates, $context);
